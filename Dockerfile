FROM python:3.9-slim-buster

RUN mkdir /stensul
WORKDIR /stensul

ADD . /stensul/
RUN apt-get update && apt-get install wget gnupg gnupg2 gnupg1 -y && rm -rf /var/lib/apt/lists/*
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
RUN apt-get update && apt-get install -y google-chrome-stable curl && rm -rf /var/lib/apt/lists/*
#pip
RUN pip install --upgrade pip
RUN pip --no-cache-dir install -r /stensul/requirements.txt
RUN pytest -v -m regression