import logging
import os
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from utils import driver_config
from utils.driver_config import get_driver_instance, config_implicit_timeout, config_browser


def locator_by(selector):
    strategy = selector.get('BY')
    locator = selector.get('LOCATOR')
    return strategy, locator


class WebDriverActions:
    ENV = os.getenv('env')

    def __init__(self, driver):
        self.driver = driver
        explicit_timeout = driver_config.config_explicit_timeout()
        self.wait = WebDriverWait(driver=self.driver, timeout=explicit_timeout)

    def is_desktop_driver(self):
        return os.environ["driver_type"] == 'desktop'

    def navigate_to(self, url: str):
        """Navigates to an URL"""
        logging.info(f"NAVIGATE TO {url}")
        get_driver_instance().get(url)

    def get_url(self):
        return get_driver_instance().current_url

    def get_element(self, locator: str):
        """Gets an element based on a selector and a strategy"""
        strategy, selector = locator
        logging.info(f"FIND ELEMENT {selector} BY {strategy}")
        self.__treatment_for_safari(locator)
        element = self.driver.find_element(strategy, selector)
        return element

    def get_elements(self, locator: str):
        """Gets an array of elements based on a selector and a strategy"""
        strategy, selector = locator
        logging.info(f"FIND ELEMENT {selector} BY {strategy}")
        self.__treatment_for_safari(locator)
        elements = self.driver.find_elements(strategy, selector)
        return elements

    def __treatment_for_safari(self, locator):
        if config_browser() == 'safari':
            get_driver_instance().implicitly_wait(1)
            try:
                strategy, selector = locator
                local_wait = WebDriverWait(driver=self.driver, timeout=5)
                local_wait.until(expected_conditions.presence_of_element_located((strategy, selector)))
                local_wait = WebDriverWait(driver=self.driver, timeout=5)
                local_wait.until(expected_conditions.element_to_be_clickable((strategy, selector)))
            except:
                pass
            get_driver_instance().implicitly_wait(config_implicit_timeout())

    def is_text_equal(self, element_text, expected_text):
        if config_browser() == 'safari':
            return element_text == str(expected_text).replace('\n', '')
        else:
            return element_text == expected_text

    def wait_for_element(self, locator: str):
        """Waits for an element based on a selector and a strategy"""
        strategy, selector = locator
        logging.info(f"WAIT FOR ELEMENT {selector}")
        return self.wait.until(expected_conditions.presence_of_element_located((strategy, selector)))

    def element_exist(self, locator: str):
        """Checks the existence of an element based on a selector and a strategy"""
        logging.info("CHECK ELEMENT EXISTENCE")
        elements = self.get_elements(locator)
        if len(elements) == 0:
            return False
        else:
            return True

    def get_element_attribute(self, locator: str, attribute):
        """Gets an attribute element based on a selector and attribute name"""
        strategy, selector = locator
        logging.info(f"FIND ATTRIBUTE {attribute} ON ELEMENT {selector}")
        elements = self.get_elements(locator)
        if len(elements) == 0:
            return ""
        else:
            return elements[0].get_attribute(attribute)

    def click_element(self, locator: str):
        """Click an element based on a selector and a strategy"""
        strategy, selector = locator
        logging.info(f"CLICK ON ELEMENT {selector}")
        elements = self.get_elements(locator)
        if len(elements) != 0:
            elements[0].click()
        else:
            raise Exception(f'No se encontró el elemento {selector}')

    def send_keys_to_element(self, locator: str, text):
        """Send keys to element based on a selector and a strategy"""
        strategy, selector = locator
        logging.info(f"SEND KEYS {text} TO ELEMENT {selector}")
        elements = self.get_elements(locator)
        if len(elements) != 0:
            elements[0].send_keys(text)
        else:
            raise Exception(f'No se encontró el elemento {selector}')

    def upload_file(self,  locator: str, path):
        """Send keys to element based on a selector and a strategy"""
        strategy, selector = locator
        logging.info(f"SEND KEYS TO ELEMENT {selector}")
        elements = self.get_elements(locator)
        if len(elements) != 0:
            elements[0].send_keys(os.path.abspath(path))
        else:
            raise Exception(f'No se encontró el elemento {selector}')

    def scroll_to_the_top_of_the_page(self):
        self.driver.execute_script("window.scrollTo(0,document.body.scrollTop)")
