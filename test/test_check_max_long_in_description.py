import time
import pytest


@pytest.mark.regression
class TestCheckMaxLongInDescription:
    """
    In this class you can find all the cases that test check max long in description.
    """

    def test_verify_check_max_long(self, open_page_herokuapp):
        """
        Test: Check max long in description

        Preconditions:
            - The user need to stay in home page

        Steps:
            - Open home page -> insert photo -> insert 301 caracters in title of movie -> Check if button is disabled
        """
        item_details_page = open_page_herokuapp
        assert item_details_page.is_displayed()
        item_details_page.select_file()
        item_details_page.insert_text("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaK")
        assert item_details_page.check_max_long()
        time.sleep(5)