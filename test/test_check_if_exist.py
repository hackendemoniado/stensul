import time
import pytest
from pages.list_of_items_page import PageListOfItems


@pytest.mark.regression
class TestCheckIfExist:
    """
    In this class you can find all the cases that test check if exist in the list the item with text “Creators: Matt Duffer, Ross Duffer”
    """

    def test_verify_check_if_exist(self, open_page_herokuapp):
        """
        Test: Check if exist in the list the item with text “Creators: Matt Duffer, Ross Duffer”

        Preconditions:
            - The user need to stay in home page

        Steps:
            - Open home page -> insert photo -> search the item -> Check if exists
        """
        item_details_page = open_page_herokuapp
        assert item_details_page.is_displayed()
        list_items_page = PageListOfItems()
        assert list_items_page.is_displayed()
        assert list_items_page.search_item("Creators: Matt Duffer, Ross Duffer")
        time.sleep(5)
