import time
import pytest
from pages.list_of_items_page import PageListOfItems


@pytest.mark.regression
class TestCreateAnItem:
    """
    In this class you can find all the cases that test the creation of a new article.
    """

    def test_verify_create_an_item(self, open_page_herokuapp):
        """
        Test: Create a new item

        Preconditions:
            - The user need to stay in home page

        Steps:
            - Open home page -> insert photo -> insert title of movie -> click on Create Item -> validate if It's item exists
        """
        item_details_page = open_page_herokuapp
        assert item_details_page.is_displayed()
        item_details_page.select_file()
        item_details_page.insert_text("MR. ROBOT.")
        item_details_page.click_in_button_create_item()
        list_items_page = PageListOfItems()
        assert list_items_page.is_displayed()

        assert list_items_page.search_movie()
        time.sleep(5)
