import time
import pytest
from pages.list_of_items_page import PageListOfItems
from pages.warning_page import PageWarning


@pytest.mark.regression
class TestDeleteTheItemCreated:
    """
    In this class you can find all the cases that test the item was deleted.
    """

    def test_verify_delete_the_item_created(self, open_page_herokuapp):
        """
        Test: Delete the item created

        Preconditions:
            - Item need exist

        Steps:
            - Open home page -> search item -> delete item
        """
        item_details_page = open_page_herokuapp
        assert item_details_page.is_displayed()
        list_items_page = PageListOfItems()
        assert list_items_page.is_displayed()
        assert list_items_page.delete_item()
        warning = PageWarning()
        assert warning.is_displayed()
        warning.click_to_delete_item()
        time.sleep(5)
