import time
import pytest
from pages.list_of_items_page import PageListOfItems


@pytest.mark.regression
class TestEditAnotherExistingItem:
    """
    In this class you can find all the cases that test the edition of another existing article.
    """

    def test_verify_edit_another_existing_item(self, open_page_herokuapp):
        """
        Test: Edit antoher existing item

        Preconditions:
            - Item need exist

        Steps:
            - Open home page -> search item -> edit item -> validate if item was edited
        """
        item_details_page = open_page_herokuapp
        assert item_details_page.is_displayed()
        list_items_page = PageListOfItems()
        assert list_items_page.is_displayed()
        assert list_items_page.edit_item()
        item_details_page.insert_text("sergio_gonzalez")
        list_items_page.save_edited_item()
        time.sleep(5)