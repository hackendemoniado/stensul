import pytest

from pages.item_details_page import PageItemDetails


@pytest.fixture()
def open_page_herokuapp(driver):
    login_page = PageItemDetails()
    login_page.go()
    assert login_page.is_displayed()
    return login_page
