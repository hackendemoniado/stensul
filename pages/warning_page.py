import logging
import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env

from utils.web_driver_actions import locator_by


class PageWarning(BasePage):

    __WARNING_PAGE = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//h3[contains(text(),'Warning!')]"
    })

    __BUTTON_YES_DELETE_ITEM = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(text(),'Yes, delete it!')]"
    })

    def __init__(self):
        super().__init__()

    def go(self):
        servicios_url = read_config_from_current_env('home_page')
        self.navigate_to(url=servicios_url)

    def is_displayed(self):
        try:
            self.wait_for_element(self.__WARNING_PAGE)
            return True
        except:
            logging.ERROR("It's not appear the warning page.")
            return False

    def click_to_delete_item(self):
        self.wait_for_element(self.__BUTTON_YES_DELETE_ITEM)
        time.sleep(3)
        self.click_element(self.__BUTTON_YES_DELETE_ITEM)
