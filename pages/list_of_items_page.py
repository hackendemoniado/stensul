import logging
import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env

from utils.web_driver_actions import locator_by


class PageListOfItems(BasePage):
    __PAGE_LIST_OF_ITEMS = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//h1[contains(text(),'List of items')]"
    })

    __NEW_MOVIE = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//p[contains(text(),'MR. ROBOT.')]"
    })

    __ITEM_TO_EDIT = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//p[contains(text(),'Mike hides the mysterious girl in his house. Joyce gets a strange phone call.')]"
    })

    __BUTTON_EDIT_ITEM = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "(//button[contains(text(),'Edit')])[2]"
    })

    __BUTTON_UPDATE_ITEM = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[contains(text(),'Update Item')]"
    })

    __BUTTON_DELETE_ITEM = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "(//button[contains(text(),'Delete')])[14]"
    })

    __LIST_OF_ITEMS = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//p[@class='story ng-binding']"
    })

    def __init__(self):
        super().__init__()

    def go(self):
        servicios_url = read_config_from_current_env('home_page')
        self.navigate_to(url=servicios_url)

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PAGE_LIST_OF_ITEMS)
            return True
        except:
            logging.ERROR("It's not appear the list of items page.")
            return False

    def search_movie(self):
        return self.element_exist(self.__NEW_MOVIE)

    def __search_item_to_edit(self):
        if self.element_exist(self.__ITEM_TO_EDIT):
            return True
        else:
            return False

    def __search_item_to_delete(self):
        if self.element_exist(self.__NEW_MOVIE):
            return True
        else:
            return False

    def edit_item(self):
        if self.__search_item_to_edit():
            self.wait_for_element(self.__BUTTON_EDIT_ITEM)
            self.click_element(self.__BUTTON_EDIT_ITEM)
            time.sleep(5)
            return True
        else:
            return False

    def save_edited_item(self):
        self.wait_for_element(self.__BUTTON_EDIT_ITEM)
        self.click_element(self.__BUTTON_UPDATE_ITEM)

    def delete_item(self):
        if self.__search_item_to_delete():
            self.wait_for_element(self.__BUTTON_DELETE_ITEM)
            self.click_element(self.__BUTTON_DELETE_ITEM)
            self.scroll_to_the_top_of_the_page()
            time.sleep(3)
            return True
        else:
            return False

    def search_item(self, item):
        self.wait_for_element(self.__LIST_OF_ITEMS)
        self.items = self.get_elements(self.__LIST_OF_ITEMS)
        i = 0
        while i < len(self.items):
            if self.items[i].text == item:
                return True
            else:
                i = i + 1

        if i == len(self.items):
            return False