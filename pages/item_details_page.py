import logging
import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env

from utils.web_driver_actions import locator_by


class PageItemDetails(BasePage):
    __PAGE_ITEM_DETAILS = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//h3[contains(text(),'Item Details')]"
    })

    __SELECT_FILE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'inputImage'
    })

    __INSERT_TEXT = locator_by({
        'BY': By.NAME,
        'LOCATOR': 'text'
    })

    __BUTTON_CREATE_ITEM = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@class='btn pull-right btn-success']"
    })

    def __init__(self):
        super().__init__()

    def go(self):
        servicios_url = read_config_from_current_env('home_page')
        self.navigate_to(url=servicios_url)

    def is_displayed(self):
        try:
            self.wait_for_element(self.__PAGE_ITEM_DETAILS)
            return True
        except:
            logging.ERROR("It's not appear the item details page.")
            return False

    def select_file(self):
        self.wait_for_element(self.__SELECT_FILE)
        self.upload_file(self.__SELECT_FILE, "test/photos/mr_robot.jpg")

    def insert_text(self, text=""):
        self.wait_for_element(self.__INSERT_TEXT)
        self.send_keys_to_element(self.__INSERT_TEXT, text)
        time.sleep(3)

    def click_in_button_create_item(self):
        self.wait_for_element(self.__BUTTON_CREATE_ITEM)
        self.click_element(self.__BUTTON_CREATE_ITEM)

    def check_max_long(self):
        self.wait_for_element(self.__BUTTON_CREATE_ITEM)
        disabled = self.get_element_attribute(self.__BUTTON_CREATE_ITEM,"disabled")
        if disabled:
            return True
        else:
            return False
